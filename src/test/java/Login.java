import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;



import io.github.bonigarcia.wdm.WebDriverManager;

public class Login {

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();


		driver.get("https://stag.taj.services/tajwebdoctor/#/tajwebdoctor/login");


		//Login
		WebElement username=driver.findElement(By.id("mobileNo"));
		username.sendKeys("3009510819");
		WebElement password=driver.findElement(By.id("userPin"));
		password.sendKeys("1111");
		WebElement login=driver.findElement(By.xpath("//button[@type='submit']"));
		login.click();

		Thread.sleep(3000);
		WebElement popuplogin=driver.findElement(By.xpath("//div[@id='confirmationModal']/div/button"));

		SoftAssert assertion= new  SoftAssert();


		assertion.assertTrue(popuplogin.isDisplayed());
		try
		{
			popuplogin.click();
		}
		catch (Exception e) {
			System.out.println("pop not displayed");
		}

		Thread.sleep(3000);
		WebElement versionpopup = driver.findElement(By.xpath("//app-confirmation-modal/div/div/button"));
		Thread.sleep(3000);
		JavascriptExecutor jse = (JavascriptExecutor) driver; jse.executeScript("window.alert = function() {};");
		JavascriptExecutor jalert = (JavascriptExecutor) driver; jalert.executeScript("window.confirm = function() {};");
		assertion.assertTrue(versionpopup.isDisplayed());
		try
		{
			versionpopup.click();
		}
		catch (Exception e) {
			System.out.println("pop not displayed");
		}
		jse.executeScript("window.alert = function() {};");
		jalert = (JavascriptExecutor) driver; jalert.executeScript("window.confirm = function() {};");
		Thread.sleep(3000);
		WebElement contactus =driver.findElement(By.xpath("//html/body/app-root/app-secure/div[2]/app-top-bar/div/div/div/nav/ul/li[5]/a"));
		Thread.sleep(3000);
		contactus.click();
		Thread.sleep(3000);



		driver.quit();

	}
}
